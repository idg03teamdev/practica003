import "./App.css";
import { miTitulo } from "./components/miTitulo";
import { subTitulo } from "./components/subTitulo";
function App() {
  return (
    <div className="App">
      <header className="App-header">
        <miTitulo />
        <subTitulo />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
      </header>
    </div>
  );
}

export default App;
